
// Bài 1: tính tiền lương nhân viên
/**
 * input:
 * - Số ngày làm
 * - lương một ngày: 100000
 * step
 * - tạo ra biến số ngày làm
 * - áp dung công thức tính tiền lương đã cho
 * output
 * - tiền lương     
 */

var workDay = 5;

var result1 = workDay * 100000;
console.log('result1: ', result1);

// bài 2: tính giá trị trung bình
/**
 * input: 5 số thực
 * 
 * step:
 * - tạo ra 5 biến 5 số thực 
 * -tình giá trị trung bình của 5 số thực bằng cách lấy tổng chia cho 5
 * 
 * output: số trung bình
 */

var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;

var result2 = (a + b + c + d + e) / 5;
console.log('result2: ', result2);

// bài 3: QUy đổi tiền
/**
 * input
 * - Số usd mà người dung nhập
 * - giá usd là 23500 VND
 * step
 * - tạo biến số usd mà người dùng nhập
 * - lấy số usd mà người dùng nhập nhân cho 23500 VND
 * output
 * - số tiền sau khi đc quy đổi
 */

var money = 2;

var result3 = money * 235000;
console.log('result3: ', result3);

// bải 4: tính diện tích chu vi hình chữ nhật
/**
 * input
 * - 2 cạnh của hình chữ nhật
 * step
 * - tạo ra 2 biến chứa 2 cạnh của hình chữ nhật
 * - áp dụng công thức tính chu vi và diện tích cho ra kết quả
 * output
 * - chu vi hình chữ nhặt
 * - diện tích hình chữ nhật
 */

var edge1 = 6;
var edge2 = 8;

var result4_1 = (edge1 + edge2) * 2;
console.log('result4_1: ', result4_1);

var result4_2 = edge1 * edge2;
console.log('result4_2: ', result4_2);

// bài 5: tính tổng 2 ký số
/**
 * input: số có 2 chữ số
 * 
 * step
 * - tạo ra biến chứa 1 số bất kỳ có 2 chữ số
 * - lấy số hàng đơn vị bằng cách chia lấy dư cho 10
 * - lấy số hàng chục bằng cách chia cho 10 (lấy số làm tròn xuống)
 * - tính tống của 2 số vừa lấy được
 * 
 * output: tổng của 2 ký số
 * 
 */

var number = 99;

var donVi = number % 10;
var hangChuc = Math.floor(number / 10);

var sum = donVi + hangChuc;
console.log('sum: ', sum);

